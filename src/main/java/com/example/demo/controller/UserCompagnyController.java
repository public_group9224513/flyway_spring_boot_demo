package com.example.demo.controller;

import com.example.demo.entity.UserCompagny;
import com.example.demo.service.IUserCompagnyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/compagny")
public class UserCompagnyController {

        @Autowired
        IUserCompagnyService iUserCompagnyService;

        @GetMapping("/all")
        public ResponseEntity<List<UserCompagny>> getAllUserCompagny() {
                return new ResponseEntity<>(iUserCompagnyService.findAll(), HttpStatus.OK);
        }

        @GetMapping("")
        public ResponseEntity<UserCompagny> getUserCompagnyById(@RequestParam long id) {
            Optional<UserCompagny> userCompagnyData = iUserCompagnyService.findById(id);
            return userCompagnyData.map(userCompagny -> new ResponseEntity<>(userCompagny, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
        }


}
