package com.example.demo.controller;

import com.example.demo.entity.Etudiant;
import com.example.demo.service.IEtudiantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class EtudiantController {

        @Autowired
        IEtudiantService iEtudiantService;

        @GetMapping("/")
        public ResponseEntity<String> getMenu() {
            String textResponse="TRY <a href=\"http://localhost:8080/etudiant/all\">http://localhost:8080/etudiant/all</a>";
            textResponse=textResponse+" OR <a href=\"http://localhost:8080/etudiant?id=1\">http://localhost:8080/etudiant?id=1</a> </br>";
            textResponse=textResponse+" OR <a href=\"http://localhost:8080/compagny/all\">http://localhost:8080/compagny/all</a> </br>";
            textResponse=textResponse+" OR <a href=\"http://localhost:8080/compagny?id=1\">http://localhost:8080/compagny?id=1</a> </br>";
            textResponse=textResponse+"To access H2 Database console (see readme.md for url and credentials) : <a href=\"http://localhost:8080/h2-console\">http://localhost:8080/h2-console</a> </br>";
            return new ResponseEntity<>(textResponse, HttpStatus.OK);
    }

        @GetMapping("/etudiant/all")
        public ResponseEntity<List<Etudiant>> getAllEtudiants() {
                return new ResponseEntity<>(iEtudiantService.findAll(), HttpStatus.OK);
        }

        @GetMapping("/etudiant")
        public ResponseEntity<Etudiant> getEtudiantById(@RequestParam long id) {
            Optional<Etudiant> etudiantData = iEtudiantService.findById(id);
            return etudiantData.map(etudiant -> new ResponseEntity<>(etudiant, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
        }


}
