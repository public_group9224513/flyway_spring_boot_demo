package com.example.demo.repository;

import com.example.demo.entity.UserCompagny;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
 public interface IUserCompagnyRepository extends JpaRepository<UserCompagny,Long> {

}
