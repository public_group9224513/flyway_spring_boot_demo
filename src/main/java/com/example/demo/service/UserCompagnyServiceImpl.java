package com.example.demo.service;

import com.example.demo.entity.UserCompagny;
import com.example.demo.repository.IUserCompagnyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserCompagnyServiceImpl implements IUserCompagnyService {

    @Autowired
    IUserCompagnyRepository iuserCompagnyRepository;

    @Override
    public List<UserCompagny> findAll() {
        return iuserCompagnyRepository.findAll();
    }

    @Override
    public Optional<UserCompagny> findById(Long id) {
        return iuserCompagnyRepository.findById(id);
    }
}
