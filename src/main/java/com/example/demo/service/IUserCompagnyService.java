package com.example.demo.service;

import com.example.demo.entity.UserCompagny;
import java.util.List;
import java.util.Optional;

public interface IUserCompagnyService {

    List<UserCompagny> findAll();

    Optional<UserCompagny> findById(Long id);
}
