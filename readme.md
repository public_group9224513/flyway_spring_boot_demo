POC FLYWAY

## Resources

### Maven
- artifact : https://mvnrepository.com/artifact/org.flywaydb/flyway-core

### H2 databse
- all commands : https://h2database.com/html/commands.html?highlight=drop%2Calias&search=drop%20alias#set_schema

### Spring and Flyway
- properties : https://docs.spring.io/spring-boot/docs/current/reference/html/application-properties.html#appendix.application-properties.data-migration
- springboot integration : https://documentation.red-gate.com/fd/community-plugins-and-integrations-spring-boot-184127423.html

## Compatibilty notes
- starting with Spring Boot 3.0, Java 17 is ***required***
- Spring Boot 2.7 : 0

## Flyway principles

- flyway execute sql sequences BEFORE spring boot operate.
- flyway_schema_history table is exclusively for use by Flyway. Never edit or update it directly.


## Implementation : adding flyway core

```xml
<dependency>
    <groupId>org.flywaydb</groupId>
    <artifactId>flyway-core</artifactId>
</dependency>
```

## Version of this POC

- spring-boot-starter-parent : 2.7.18
- jave : 1.8
- flyway.version ( auto-selected by Springboot, see effective pom.xml ) : 8.5.13
- h2.version : 2.2.224


## Trying controllers with web browser
- http://localhost:8080
- http://localhost:8080/etudiant/all
- http://localhost:8080/etudiant?id=1
- http://localhost:8080/etudiant?id=2 ...
- http://localhost:8080/compagny/all
- http://localhost:8080/compagny?id=1 ...

## Access GUI to play with database
H2 console access : http://localhost:8080/h2-console  

- Database JBDC URL : jdbc:h2:mem:testdb
- User Name : sa
- Password : ***it's an empty password, leave it empty***

<img src="h2_database_console_login.png" alt= “s1” width="60%" height="60%">

